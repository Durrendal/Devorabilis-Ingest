# Devorabilis Ingest
## MPG Video Converter
```
Devorabilis (Latin): Meaning devourable
```

Devorabilis Ingest servers to address an issue found at WMAZ wherein end users found it difficult and tedious to convert video files to MPG format in bulk.
To address this issue I wrapped an ffmpeg call in a lisp ingestion server that monitors a local directory for files to convert. From the user standpoint conversion is as simple as dragging the desired file to a shared drive, and waiting for Devorabilis to ingest and convert the media.

# Usage:

* File Structure:
```
Devorabilis looks for files at /var/devorabilis/ingest, when they're detected transcodes them and outputs to /var/devorabilis/accumulate
A ledger of transcoded files it written at /var/www/devorabilis/ledger. All three of these paths are necessary for devorabilis to run.
```

* Use:
### Currently only FFMPEG conversion is implemented

```
Devorabilis -- GPLv3 -- Author: Will Sinatra

Usage: Devorabilis [-h|--help] [--mpg] [--mov] [--mp4] [--mxf] [--pdf] [--jpg]
                   [--png]

Available options:
  -h, --help               Devorabilis-Ingest is meant to be run as a service
  --mpg                    Convert to MPG
  --mov                    Convert to MOV
  --mp4                    Convert to MP4
  --mxf                    Convert to MXF
  --pdf                    Convert to PDF
  --jpg                    Convert to JPG
  --png                    Convert to PNG
```

When devorabilis is started, it will immediately look for files in /var/devorabilis/ingest, if it finds any file it will create a ledger record and begin encoding it, the flag passed at start time determines the output. (ie: devorabilis --mpg)

If the file is locked (in transit, in use by another program) devorabilis will NOT process the file, it will rather throw an internal exception without erroring out or halting, and attempt to retry the conversion every 5 seconds.

# Container:

A container build can be found in this repo. It's built on Alpine Linux, and is configured to build devorabilis from this git repo's source.

https://gitlab.com/Durrendal/WS-Docker-Builds/tree/master/devorabilis