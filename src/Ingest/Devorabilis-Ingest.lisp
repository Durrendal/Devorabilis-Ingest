;; Devorabils-Ingest.lisp -- MPG Conversion Ingestion

;; Author: Will Sinatra <wpsinatra@gmail.com>

;; Dependencies:
;; uiop, ffmpeg

;; License: GPLv3

;;  This file is part of Devorabilis-Ingest.

;;    Devorabilis-Ingest is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.

;;    Devorabilis-Ingest is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU General Public License for more details.

;;    You should have received a copy of the GNU General Public License
;;    along with Devorabilis-Ingest.  If not, see <https://www.gnu.org/licenses/>.

;;;; Code:

(uiop:define-package #:devorabilis-ingest
    (:use :cl :uiop)
  (:export :dir-mon
	   :collect-files
	   :correct-paths
	   :create-accumulation
	   :write-file
	   :entern-accumulation
	   :cur-date
	   :end-date
	   :split-fstr
	   :add-config))

(in-package devorabilis-ingest)

(defvar ingestion-dir "/var/devorabilis/ingest/")
(defvar accumulation-dir "/var/devorabilis/accumulate/")
(defvar length_of_stay 7)
(defvar stay_ledger "/var/devorabilis/ledger")

;; Utility Functions
(defun cur-date ()
  (local-time:today))

(defun end-date ()
  (local-time:adjust-timestamp (local-time:today) (offset :day length_of_stay)))

(defmacro split-fstr (f-string ext)
  `(let
       ((out-name (str:split ,ext ,f-string)))
     (print out-name)))

;; Ledger Enternment Functions
(defun create-accumulation (out-name)
  (list out-name (end-date)))

(defun write-file (file content)
  (with-open-file (stream file
			  :direction :output
			  :if-exists :append
			  :if-does-not-exist :create)
    (format stream content))
  file)

(defun entern-accumulation (out-name)
  (let
      ((guest_record (create-accumulation out-name)))
    (write-file stay_ledger (format nil "~a~%" guest_record))))

;; List of devourable files
(defun collect-files (dir)
  (directory-files dir))

;; Count of devourable files
(defun devorabilis-files (files)
  (list-length files))

;; Removes #P from the path names, accesses a single path at a time
(defun correct-paths (path-name)
  (unless
      (equal path-name NIL)
    (namestring (nth 0 path-name))))

(defun add-config ()
  (if
   (equal (probe-file "/etc/devorabilis/devourable.cfg") NIL)
   (progn
     ;;Add syslog input
     (format t "No configuration file exists for devorabilis at /etc/devorabilis, this program cannot run without configuration~%")
     (sb-ext:quit))
   (progn
     (load #P"/etc/devorabilis/devourable.cfg"))))

(defun ensure-structure ()
  (ensure-directories-exist "/var/devorabilis/")
  (ensure-directories-exist "/var/devorabilis/ingest/")
  (ensure-directories-exist "/var/devorabilis/accumulate/"))
   
;; Converts files in Ingestion to declared out-type with ffmpeg, exports to Accumulation
;; TODO Migrate (peredo) to Devorabilis-Conversion to handle multiple file type ingestions, this means extending (dir-mon) with an additioanl conversion type arg, or a big (cond) statement, either or.
(defun peredo (in-name out-type)
  (let
      ((fstr (file-namestring in-name)))
    (let
	((out-name
	  (concatenate 'string (car (split-fstr fstr (concatenate 'string "." (nth 1 (split-fstr fstr "."))))) "." out-type)))
      (progn
	(format t "~%Processing Devorabilis Files~%")
       ;(run-program (concatenate 'string "ffmpeg -y -i " in-name " -c:v libx264 -c:a copy -c:s -a53cc -qscale 0 " accumulation-dir out-name))
	(run-program (concatenate 'string "ffmpeg -y -i " in-name " -qscale 0 " accumulation-dir out-name))
	(entern-accumulation out-name)
	(format t "Accumulating Devorabilis Files~%")
	(run-program (concatenate 'string "rm " in-name))))))

(defun peredo-new (in-name)
  (let
      ((fstr (file-namestring in-name)))
    (let
	((in-type (nth 1 fstr)))
      (print in-type)
      (let
	  ((out-type
	    (getf *devourable* in-type)))
	(print out-type)
	(let
	    ((out-name
	      (concatenate 'string (car (split-fstr fstr (concatenate 'string "." (nth 1 (split-fstr fstr "."))))) "." out-type)))
	  (progn
	    (format t "~%Processing Devorabilis Files~%")
	    (run-program (concatenate 'string "ffmpeg -y -i " in-name " -c copy -map 0 -qscale 0 " accumulation-dir out-name))
	    (entern-accumulation out-name)
	    (format t "Accumulating Devorabilis Files~%")
	    (run-program (concatenate 'string "rm " in-name))))))))

;; Monitors /var/devorabilis/ingest for new video files, checks every 90s for new files, if they exist it runs peredo to convert them
(defun dir-mon (out-type)
  (add-config)
  (ensure-structure)
  (loop
     (let
	 ((devorabilis-c (devorabilis-files (collect-files ingestion-dir))))
       (cond
	 ((eq devorabilis-c NIL)
	  (progn
	    (format t "The beast sleeps, there is nothing left to devour~%")
	    (sleep 90)))
	 ((<= devorabilis-c 0)
	  (progn
	    (format t "The beast sleeps, there is nothing left to devour~%")
	    (sleep 90)))
	 ((>= devorabilis-c 1)
	  (progn
	    (cond
	      ((equal (list-length (collect-files ingestion-dir)) 0)
	       (format t "The beast sleeps, there is nothing left to devour~%")
	       (sleep 90))
	      ((>= (list-length (collect-files ingestion-dir)) 1)
	       (progn
		 (let
		     ((f-type (nth 1 (split-fstr (file-namestring (correct-paths (collect-files ingestion-dir))) "."))))
		   (if (equal out-type f-type)
		       (progn
			 (format t "~%Devorabilis File Was Already Consumed!~%")
			 (run-program (concatenate 'string "mv " (correct-paths (collect-files ingestion-dir)) " " accumulation-dir))
			 (format t "Devorabilis File Accumulated~%")
			 (sleep 5))
		       (progn
			 (handler-case
			     (peredo (correct-paths (collect-files ingestion-dir)) out-type)
			   (error (c)
			     (values nil c)))
			 (sleep 5)))))))))))))
