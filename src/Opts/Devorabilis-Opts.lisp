;; Devorabilis-Opts.lisp -- Devorabilis Unix Options

;; Author: Will Sinatra <wpsinatra@gmail.com>

;; Dependencies:
;; uiop

;; License: GPLv3                                                                                                                                          
                                                                                                                                                           
;;  This file is part of Devorabilis-Ingest.                                                                                                               
                                                                                                                                                           
;;    Devorabilis-Ingest is free software: you can redistribute it and/or modify                                                                           
;;    it under the terms of the GNU General Public License as published by                                                                                 
;;    the Free Software Foundation, either version 3 of the License, or                                                                                    
;;    (at your option) any later version.                                                                                                                  
                                                                                                                                                           
;;    Devorabilis-Ingest is distributed in the hope that it will be useful,                                                                                
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                       
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                        
;;    GNU General Public License for more details.                                                                                                         
                                                                                                                                                           
;;    You should have received a copy of the GNU General Public License                                                                                    
;;    along with Devorabilis-Ingest.  If not, see <https://www.gnu.org/licenses/>.

;;;; Code:

(uiop:define-package #:devorabilis-opts
    (:use :cl :uiop :devorabilis-ingest :devorabilis-sanitize)
  (:export :main))

(in-package devorabilis-opts)

(opts:define-opts
 (:name :help
	:description "Devorabilis-Ingest is meant to be run as a service"
	:short #\h
	:long "help")
 (:name :mpg
	:description "Convert to MPG"
	:long "mpg")
 (:name :mov
	:description "Convert to MOV"
	:long "mov")
 (:name :mp4
	:description "Convert to MP4"
	:long "mp4")
 (:name :mxf
	:description "Convert to MXF"
	:long "mxf")
 (:name :daemon
	:description "Convert all types based on configuration"
	:long "daemon"))

(defun unknown-option (condition)
  (format t "warning: ~s option is unknown!~%" (opts:option condition))
  (invoke-restart 'opts:skip-option))

(defmacro when-option ((options opt) &body body)
  `(let ((it (getf ,options ,opt)))
     (when it
       ,@body)))

(defun main ()
  (multiple-value-bind (options)
      (handler-case
	  (handler-bind ((opts:unknown-option #'unknown-option))
	    (opts:get-opts))
	(opts:missing-arg (condition)
	  (format t "fatal: option ~s needs an argument!~%"
		  (opts:option condition)))
	(opts:arg-parser-failed (condition)
	  (format t "fatal: cannot parse ~s as argument of ~s~%"
		  (opts:raw-arg condition)
		  (opts:option condition)))
	(opts:missing-required-option (con)
	  (format t "fatal: ~a~%" con)
	  (opts:exit 1)))
    (when-option (options :help)
		 (opts:describe
		  :prefix "Devorabilis -- GPLv3 -- Author: Will Sinatra"
		  :usage-of "Devorabilis"))
    (when-option (options :mpg)
		 (sb-thread:make-thread (dir-mon "mpg"))
		 (sb-thread:make-thread (sanitize-accumulation)))
    (when-option (options :mov)
		 (sb-thread:make-thread (dir-mon "mov"))
		 (sb-thread:make-thread (sanitize-accumulation)))
    (when-option (options :mp4)
		 (sb-thread:make-thread (dir-mon "mp4"))
		 (sb-thread:make-thread (sanitize-accumulation)))
    (when-option (options :mxf)
		 (sb-thread:make-thread (dir-mon "mxf"))
		 (sb-thread:make-thread (sanitize-accumulation)))
    (when-option (options :pdf)
		 (sb-thread:make-thread (dir-mon "pdf"))
		 (sb-thread:make-thread (sanitize-accumulation)))
    (when-option (options :jpg)
		 (sb-thread:make-thread (dir-mon "jpg"))
		 (sb-thread:make-thread (sanitize-accumulation)))
    (when-option (options :png)
		 (sb-thread:make-thread (dir-mon "png"))
		 (sb-thread:make-thread (sanitize-accumulation)))
    (when-option (options :daemon)
		 (add-config)
		 (sb-thread:make-thread (dir-mon "all"))
		 (sb-thread:make-thread (sanitize-accumulation)))))
