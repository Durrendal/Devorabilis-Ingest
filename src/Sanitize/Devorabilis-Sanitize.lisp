;; Devorabils-Sanitize.lisp -- Devorabilis Directory Sanitization

;; Author: Will Sinatra <wpsinatra@gmail.com> 

;; Dependencies:
;; uiop

;; License: GPLv3

;;  This file is part of Devorabilis-Ingest

;;    Devorabilis-Ingest is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.

;;    Devorabilis-Ingest is distributed in the hope that it will be useful, 
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of 
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU General Public License for more details.

;;    You should have received a copy of the GNU General Public License 
;;    along with Devorabilis-Ingest.  If not, see <https://www.gnu.org/licenses/>.

;;;; Code:

(uiop:define-package #:devorabilis-sanitize
    (:use :cl :uiop :devorabilis-ingest)
  (:export :check-accumulation
	   :sanitize-accumulation))

(in-package devorabilis-sanitize)

(defvar stay_ledger "/var/devorabilis/ledger")

(defun check-accumulation ()
  (with-open-file (stream stay_ledger)
    (do ((line (read-line stream nil)
		(read-line stream nil)))
	((null line))
      (print line))))

(defun sanitize-accumulation ()
  (loop
       (let
	   ((ledger (check-accumulation)))
	 (let
	     ((end-date (nth 1 (list ledger)))
	      (file (nth 0 (list ledger)))
	      (today (cur-date)))
	   (cond
	     ((equal end-date today)
	      (progn
		(run-program (concatenate 'string "rm " file)))))))
     (sleep 720)))
