#!/bin/sh

build() {
    sbcl --non-interactive \
	 --eval '(require "asdf")' \
	 --eval '(ql:quickload "uiop")' \
	 --eval '(ql:quickload "unix-opts")' \
	 --eval '(ql:quickload "local-time")' \
	 --eval '(ql:quickload "sb-concurrency")' \
	 --eval '(ql:quickload "str")' \
	 --eval '(asdf:operate (quote asdf:load-op) (quote devorabilis))' \
	 --eval '(save-lisp-and-die "devorabilis" :executable t :toplevel (quote devorabilis-opts:main))'
}

build
