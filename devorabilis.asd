;;; Devorabilis.asd

;; License: GPLv3                                                                                                                                          
                                                                                                                                                           
;;  This file is part of Devorabilis-Ingest.                                                                                                               
                                                                                                                                                           
;;    Devorabilis-Ingest is free software: you can redistribute it and/or modify                                                                           
;;    it under the terms of the GNU General Public License as published by                                                                                 
;;    the Free Software Foundation, either version 3 of the License, or                                                                                    
;;    (at your option) any later version.                                                                                                                  
                                                                                                                                                           
;;    Devorabilis-Ingest is distributed in the hope that it will be useful,                                                                                
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of                                                                                       
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                                                                                        
;;    GNU General Public License for more details.                                                                                                         
                                                                                                                                                           
;;    You should have received a copy of the GNU General Public License                                                                                    
;;    along with Devorabilis-Ingest.  If not, see <https://www.gnu.org/licenses/>.

(defpackage #:devorabilis
  (:use :cl :uiop :asdf))

(asdf:defsystem #:devorabilis
		:description "Devourable Ingestion Service"
		:author "Will Sinatra <wpsinatra@gmail.com>"
		:license "GPLv3"
		#+asdf-unicode :encoding #+asdf-unicode :utf-8
		:serial NIL
		:depends-on (#:uiop
			     #:unix-opts
			     #:local-time
			     #:sb-concurrency
			     #:str)
		:components ((:file "src/Ingest/Devorabilis-Ingest")
			     (:file "src/Sanitize/Devorabilis-Sanitize")
			     (:file "src/Opts/Devorabilis-Opts")))
